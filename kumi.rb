class Kumi
  require './kyotu'
  include Kyotu
  attr_reader :h_kumi

  def initialize db, arr2
    @db = db
    set_const
    h_data = read_all arr2
    @h_kumi = edit_kumi h_data
    db_save
    @h_kumi
  end

  def set_const
    @n_sho_code = 1
    @n_sN1 = 2
    @n_aza_cd = 4
    @n_chiban = 5
    @n_block = 6
    @n_lot = 7
    @n_shitei = 13
  end


  def read_all arr2
    h_data = {}
    arr2.each do |arr|
      sho_code = arr[@n_sho_code]
      sN1 = arr[@n_sN1]
      ctl_key = format("%s%02d",sho_code, sN1)
      if h_data[ctl_key]
        h_data[ctl_key].push(arr)
      else
        h_data[ctl_key] = [arr]
      end
    end
    h_data
  end

  def edit_kumi h_data
    h_kumi = {}
    h_data.keys.each do |key|
      h_data[key].each do |arr|
        h_kumi[key] = [[],[],[]] unless h_kumi[key]
        wk_aza = arr[@n_aza_cd]
        wk_aza = "" if wk_aza.nil?
        if wk_aza[/^(\d\d)\d$/]
          aza_cd = $1
          wk_chiban = arr[@n_chiban]
          chiban_cd = chiban_cd_edit wk_chiban
          f_code = aza_cd + chiban_cd
          h_kumi[key][0].push(f_code)
        end
        kan_cd = kanchi_cd_edit(arr[@n_block], arr[@n_lot])
        h_kumi[key][1].push(kan_cd) if kan_cd
        arr_shitei = arr[@n_shitei,3]
        arr_shitei = ["","",""] unless arr_shitei
        h_kumi[key][2] = arr_shitei unless arr_shitei[0] == ""
      end
    end
    h_kumi
  end

  def db_save
    @h_kumi.keys.sort.each do |key|
      arr_ju, arr_kan, arr_memo = @h_kumi[ key ]
      str_ju = arr_ju.join("|")
      str_kan = arr_kan.join("|")
      str_memo = arr_memo.join("|")
      sql = "INSERT INTO kumi values" +
           "('#{key}','#{str_ju}','#{str_kan}','#{str_memo}')"
p sql
      @db.execute(sql)
    end
  end

#  def chiban_cd_edit wk_chiban
#    wk1, wk2 = wk_chiban.split(/-/)
#    wk2 = "0" unless wk2
#    return format("%03d%03d", wk1, wk2)
#  end
#
#  def kanchi_cd_edit blk, lot
#    blk = "" unless blk
#    return nil unless blk[/(\d+)$/]
#    blk_cd = $1
#    lot1,lot2 = lot.split(/-/)
#    lot1[/(\d+$)/]
#    if $1.nil?
#      print "error: no lot number!\n";exit
#    end
#    lot2 = "0" unless lot2
#    kan_cd = format("%02d%03d%02d", blk_cd, lot1, lot2)
#    return kan_cd
#  end

  def code_table
    arr = [ "010:01:牟呂町字大塚",
            "020:02:牟呂町字奥山",
            "030:03:牟呂町字奥山新田",
            "040:04:牟呂町字北汐田",
            "050:05:牟呂町字古田",
            "060:06:牟呂町字古幡焼",
            "070:07:牟呂町字中西",
            "080:08:牟呂町字東里",
            "090:09:牟呂町字百間",
            "100:10:牟呂町字松崎",
            "110:11:牟呂町字松島",
            "120:12:牟呂町字松島東",
            "130:13:牟呂町字松島",
            "140:14:牟呂町字南汐田",
            "190:19:神野新田町字会所前",
            "210:21:柱四番町",
            "220:22:柱五番町",
            "240:24:藤沢町",
            "250:25:潮崎町"
            ]
  end



end



