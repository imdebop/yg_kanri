class Juzen
  require 'yaml'
  require './kyotu'
  include Kyotu

  attr_reader :cho, :chiban, :chimoku, :tokiS, :shoyu,
    :kijunS, :m2shisu, :hyotei, :arr_item_cols

  def initialize db
    @db = db
    @arr_item_cols = [ [:cho, 2], [:chiban, 3], [:chimoku, 7], [:tokiS, 8],
      [:shoyu, 9], [:kijunS, 10], [:m2shisu, 11], [:hyotei, 15] ]
    @arr_cols = @arr_item_cols.map do |a| a[1] end
    @cur_data = nil
    @ju_col_cho = @arr_item_cols.assoc( :cho )[1]
    @ju_col_chiban = @arr_item_cols.assoc( :chiban )[1]
  end

  def ex2db arr
    cho = arr[0][ @ju_col_cho - 1 ]
    chiban = arr[0][ @ju_col_chiban - 1 ]
    cd = chiban_cd_gen(cho, chiban)
    data = arr.join("|")
    sql = "INSERT INTO juzen values( '#{cd}','#{data}')"
p sql
    @db.execute(sql)
  end

  def chiban_cd_gen cho, chiban
    return nil unless cho[/(\d\d)\d/]
    cho_code = $1
    chiban_code = chiban_cd_edit chiban
    cd = cho_code + chiban_code
    cd
  end
end

class Shoyu
  def initialize db
    @db = db
    @db.execute("DELETE from shoyu")
    @arr_item_cols = [ [:id, 1], [:name, 2], [:jusho, 3], [:name_gen, 6],
      [:jusho_gen, 7], [:kana, 24] ]
  end

  def ex2db arr
    cd = format("%04d",arr[0].to_i)
    kana = arr[ @arr_item_cols.assoc(:kana)[1] - 1 ]
    name = arr[ @arr_item_cols.assoc(:name)[1] - 1 ]
    data = arr.join("|")
    sql = "INSERT INTO shoyu values( '#{cd}','#{kana}', '#{name}','#{data}')"
p sql
    @db.execute(sql)
  end

end

class Kanchi
  require './kyotu'
  include Kyotu

  def initialize db
    @db = db
    @db.execute("DELETE from kanchi")
    @arr_item_cols = [ [:bl, 2], [:lot, 3], [:men, 4], [:sho_code, 5],
      [:m2sisu, 6],[:hyotei_sisu,] ]
  end

  def ex2db arr
    blk = arr[1]
    lot = arr[2]
    cd = kanchi_cd_edit blk, lot
    data = arr.join("|")
    sql = "INSERT INTO kanchi values( '#{cd}','#{data}')"
p sql
    @db.execute(sql)
  end

end

