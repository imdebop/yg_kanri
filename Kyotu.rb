module Kyotu
  def chiban_cd_edit wk_chiban
    wk1, wk2 = wk_chiban.split(/-/)
    wk2 = "0" unless wk2
    return format("%03d%03d", wk1, wk2)
  end

  def kanchi_cd_edit blk, lot
    blk = "" unless blk
    return nil unless blk[/(\d+)$/]
    blk_cd = $1
    lot1,lot2 = lot.split(/-/)
    lot1[/(\d+$)/]
    if $1.nil?
      print "error: no lot number!\n";exit
    end
    lot2 = "0" unless lot2
    kan_cd = format("%02d%03d%02d", blk_cd, lot1, lot2)
    return kan_cd
  end
end

