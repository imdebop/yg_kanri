class KanriMgr
  require './kanri_db'
  require './mgr_kumi'
  require './mgr_juzen'
  require './mgr_kanchi'
  attr_reader :obj_db, :kumi, :juzen, :kanchi
  
  def initialize
    @obj_db = KanriDB.new
    @kumi = MgrKumi.new obj_db
    @juzen = MgrJuzen.new obj_db
    @kanchi = MgrKanchi.new obj_db
  end
  
  def prepare id
    @kumi.set_id id
  end
  
  
  
  
end