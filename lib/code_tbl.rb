class CodeTbl
  attr_reader :h_cho
  def initialize
    @h_cho = h_cho
  end

  def h_cho
    h = {}
    arr = [ "010:01:牟呂町字大塚",
            "020:02:牟呂町字奥山",
            "030:03:牟呂町字奥山新田",
            "040:04:牟呂町字北汐田",
            "050:05:牟呂町字古田",
            "060:06:牟呂町字古幡焼",
            "070:07:牟呂町字中西",
            "080:08:牟呂町字東里",
            "090:09:牟呂町字百間",
            "100:10:牟呂町字松崎",
            "110:11:牟呂町字松島",
            "120:12:牟呂町字松島東",
            "130:13:牟呂町字松島",
            "140:14:牟呂町字南汐田",
            "190:19:神野新田町字会所前",
            "210:21:柱四番町",
            "220:22:柱五番町",
            "240:24:藤沢町",
            "250:25:潮崎町"
            ]
    arr.each do |data|
      cd1, cd2, cho = data.split(/:/)
      h[cd2] = cho
    end
    h
  end

  def fudecd2cho_chiban cd
      cho_cd, oyaban, eda = cd.unpack("a2a3a3")
      cho = @h_cho[cho_cd]
      chiban = cd2chiban(oyaban, eda)
    [cho, chiban]
  end

  def cd2chiban oya, eda
    wk_chi = oya.sub(/^0+/,"")
    wk_eda = eda.sub(/^0+/,"")
    str = "#{wk_chi} - #{wk_eda}"
    str
  end
  
  def kancd2blk_lot kancd
#p kancd
    if kancd.size == 6
      str = "0" + kancd
    else
      str = kancd
    end
    blk, lot, eda = str.unpack("a2a3a3")
    blk.sub!(/^0+/,"")
    lot.sub!(/^0+/,"")
    eda.sub!(/^0+/,"")
    (lot += " - " + eda) unless eda == ""
    [blk, lot]
  end

end
