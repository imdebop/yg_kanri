require 'win32ole'
#require 'yaml'
require 'sqlite3'
require './kanri_file_def'
require './kumi'
include SQLite3

#"1:MacroMenu"
#"2:Checkp"
#"3:XVL^"
#"4:]O๎nf[^"
#"5:]O๖คf[^"
#"6:ฎใf[^"
#"7:gนf[^"
#"8:R[h\"
#"9:]Of[^(๎{๖)"
#"10:gนf[^(ฎ`)"
#"11:Sheet6"

def main 
  if ARGV == []
    str_ex = "C:\\"
  else
    str_ex = "C:\\Users\\muramatu\\Documents\\๖ถ์VVXe\\"
  end
  begin
    @db = Database.new( 'c:\\yg_data\\kanri.db' )
    fname = str_ex + "วไ \\01๖ถ์์\\02Excel\\00_MainData\\MainData.xls"
    xl = WIN32OLE.new('Excel.Application')
    book = xl.Workbooks.Open( fname )
#    proc_juzen( book )
#    proc_kumi( book )
#    proc_kanchi( book )
    book.Saved = true
    book.close

    fname = str_ex +"วไ \\01๖ถ์์\\02Excel\\01_Zผ\\01Lา\\LาS๕.xls"
    book = xl.Workbooks.Open( fname )
    proc_shoyu( book )

  ensure
    if book
      book.Saved = true
      book.close
    end
  end
end

def proc_kanchi book
  #"6:ฎใf[^"
  kanchi = Kanchi.new @db

  sheet = book.Worksheets[ "ฎใf[^" ]
  nr = 11
  arr = nil
  while true
    arr = sheet.range( "A#{nr}:EH#{nr}" ).value
    break unless arr[0][1].to_s =~ /\d/
    kanchi.ex2db arr[0]
    nr += 1
  end
end

def proc_shoyu book
  shoyu = Shoyu.new @db
  sheet = book.Worksheets["Lา"]
  nr = 11
  arr = nil
  while true
    arr = sheet.range( "B#{nr}:AE#{nr}" ).value
    break unless arr[0][0].to_s =~ /\d/
    shoyu.ex2db arr[0]
    nr += 1
  end
end

def proc_juzen book
  #"4:]O๎nf[^"
  juzen = Juzen.new @db

  sh_ju = book.Worksheets[ 4 ]
  nr = 11
  arr = nil
  while true
    arr = sh_ju.range( "A#{nr}:O#{nr}" ).value
    break unless arr[0][1].to_s =~ /\d/
    juzen.ex2db arr
    nr += 1
  end
end




def proc_kumi book
  #"7:gนf[^"
  sheet = book.Worksheets[ 7 ]
  nr = 11
  arr2 = []
  while true
    rec = sheet.range( "B#{nr}:Q#{nr}" ).value
    break unless rec[0][1].to_s =~ /\d/
    arr2.push( rec[0] )
    nr += 1
  end
  kumi = Kumi.new( @db,arr2 )
end






main

