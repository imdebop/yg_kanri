#!/usr/bin/env ruby

require 'fox16'
require './main_ctl'
#require './kanri_db'

include Fox

class TabBookWindow < FXMainWindow
  include MainCtl

  def initialize(app)
    #@kanri_db = Kanri_db.new
    # Call the base class initializer first
    super(app, "Tab Book Test", :opts => DECOR_ALL, :width => 1000, :height => 600)

    # Make a tooltip
    FXToolTip.new(getApp())

    # Menubar appears along the top of the main window
    menubar = FXMenuBar.new(self, LAYOUT_SIDE_TOP|LAYOUT_FILL_X)

    # Separator
    FXHorizontalSeparator.new(self,
      LAYOUT_SIDE_TOP|LAYOUT_FILL_X|SEPARATOR_GROOVE)

    # Contents
    contents = FXHorizontalFrame.new(self,
      LAYOUT_SIDE_TOP|FRAME_NONE|LAYOUT_FILL_X|LAYOUT_FILL_Y|PACK_UNIFORM_WIDTH)
  
    # Switcher
    @tabbook = FXTabBook.new(contents,:opts => LAYOUT_FILL_X|LAYOUT_FILL_Y|LAYOUT_RIGHT)
  
    # First item is a list
    @tab1 = FXTabItem.new(@tabbook, "-所有者-", nil)
   
    ## Main window interior
    @splitter = FXSplitter.new(@tabbook, (LAYOUT_SIDE_TOP|LAYOUT_FILL_X|
      LAYOUT_FILL_Y|SPLITTER_VERTICAL))
    group1 = FXVerticalFrame.new(@splitter,
      FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH, :padding => 0, height: 50)
    group2 = FXSplitter.new(@splitter,
      FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y|SPLITTER_HORIZONTAL)
    
    
#    listframe = FXHorizontalFrame.new(group2, FRAME_THICK|FRAME_RAISED|
#      LAYOUT_FILL_Y, padding: 0, width: 200)
    @tab11_simplelist = FXList.new(group2, :opts => LIST_EXTENDEDSELECT|LAYOUT_FILL_Y, width: 100)
    str = "あいうえおかきくけこさしすせそたちつてとなにぬねのはひふへほまみむめもやゆよわ"
    0.upto(str.length - 1).each do |n|
      #print str[n],":"
      @tab11_simplelist.appendItem(str[n])
    end



##    listframe2 = FXHorizontalFrame.new(group22, FRAME_THICK|FRAME_RAISED)
#所有者リスト
    @tab12_list = FXList.new(group2, :opts => LIST_EXTENDEDSELECT|LAYOUT_FILL_X|LAYOUT_FILL_Y)
    @tab12_list.appendItem("　　　＊＊＊＊＊＊＊＊＊＊＊＊＊＊")
    @tab12_list.appendItem("　　　名前の最初のかなを選択または、")
    @tab12_list.appendItem("　　　上の欄に検索文字を入れて")
    @tab12_list.appendItem("　　　所有者を検索してください。")



      
    # 換地をテーブルに表示
    @tab2 = FXTabItem.new(@tabbook, "--換地--", nil)
    @splitter2 = FXSplitter.new(@tabbook, (LAYOUT_SIDE_TOP|LAYOUT_FILL_X|
      LAYOUT_FILL_Y|SPLITTER_VERTICAL))
    group21 = FXVerticalFrame.new(@splitter2,
      FRAME_SUNKEN|FRAME_THICK|LAYOUT_FIX_WIDTH, :padding => 0, height: 50)
    @tab2_label = FXLabel.new(group21, "Canvas Frame", nil, JUSTIFY_LEFT|LAYOUT_FILL_X)
    group22 = FXSplitter.new(@splitter2,
      FRAME_SUNKEN|FRAME_THICK|LAYOUT_FILL_X|LAYOUT_FILL_Y|SPLITTER_HORIZONTAL)
    frame_kan = FXHorizontalFrame.new(group22, FRAME_THICK|FRAME_RAISED)
    #table_kan = FXFileList.new(frame_kan, :opts => ICONLIST_EXTENDEDSELECT|LAYOUT_FILL_X|LAYOUT_FILL_Y)
    @table_kan = FXTable.new(frame_kan,
      :opts => LAYOUT_FILL_X|LAYOUT_FILL_Y,
      :padding => 2)
    rows = 20
    @table_kan.visibleRows = rows
    @table_kan.visibleColumns = 8
    @table_kan.setTableSize(50, 14)
    #table_kan.rowHeaderMode = LAYOUT_FIX_WIDTH
    @table_kan.rowHeaderWidth =  30
    arr = ["町字", "地番","地積","街区","ロット"]
    (0..4).each do |c|
      @table_kan.setColumnText(c,arr[c])
    end
    0.upto(rows - 1).each do |n|
      [0].each do |m|  #列のセルの配置設定
        @table_kan.setItemJustify(n,m,FXTableItem::LEFT)
        @table_kan.setRowJustify(n,FXTableItem::CENTER_X)
      end
      [1,2,3].each do |m|  #列のセルの配置設定
        @table_kan.setItemJustify(n,m,FXTableItem::CENTER_X)
      end
    end
    
    
    # Third item is a directory list
    @tab3 = FXTabItem.new(@tabbook, "T&ree List", nil)
    dirframe = FXHorizontalFrame.new(@tabbook, FRAME_THICK|FRAME_RAISED)
    dirlist = FXDirList.new(dirframe,
      :opts => DIRLIST_SHOWFILES|TREELIST_SHOWS_LINES|TREELIST_SHOWS_BOXES|LAYOUT_FILL_X|LAYOUT_FILL_Y)
    
    # File Menu
    filemenu = FXMenuPane.new(self)
    FXMenuCommand.new(filemenu, "&Simple List", nil,
      @tabbook, FXTabBar::ID_OPEN_FIRST+0)
    FXMenuCommand.new(filemenu, "F&ile List", nil,
      @tabbook, FXTabBar::ID_OPEN_FIRST+1)
    FXMenuCommand.new(filemenu, "T&ree List", nil,
      @tabbook, FXTabBar::ID_OPEN_FIRST+2)
    FXMenuCommand.new(filemenu, "&Quit\tCtl-Q", nil,
      getApp(), FXApp::ID_QUIT)
    FXMenuTitle.new(menubar, "&File", nil, filemenu)
    
    # Tab side
    tabmenu = FXMenuPane.new(self)
    hideShow = FXMenuCheck.new(tabmenu, "Hide/Show Tab 2")
    hideShow.connect(SEL_COMMAND) {
      if @tab2.shown?
        @tab2.hide
        @fileframe.hide
      else
        @tab2.show
        @fileframe.show
      end
      @tab2.recalc
      @fileframe.recalc
    }
    hideShow.connect(SEL_UPDATE) { hideShow.check = @tab2.shown? }
    
    FXMenuSeparator.new(tabmenu)
    
    topTabsCmd = FXMenuRadio.new(tabmenu, "&Top Tabs")
    topTabsCmd.connect(SEL_COMMAND) do
      @tabbook.tabStyle = TABBOOK_TOPTABS
      @tab1.tabOrientation = TAB_TOP
      @tab2.tabOrientation = TAB_TOP
      @tab3.tabOrientation = TAB_TOP
    end
    topTabsCmd.connect(SEL_UPDATE) do |sender, sel, ptr|
      sender.check = (@tabbook.tabStyle == TABBOOK_TOPTABS)
    end
    
    bottomTabsCmd = FXMenuRadio.new(tabmenu, "&Bottom Tabs")
    bottomTabsCmd.connect(SEL_COMMAND) do
      @tabbook.tabStyle = TABBOOK_BOTTOMTABS
      @tab1.tabOrientation = TAB_BOTTOM
      @tab2.tabOrientation = TAB_BOTTOM
      @tab3.tabOrientation = TAB_BOTTOM
    end
    bottomTabsCmd.connect(SEL_UPDATE) do |sender, sel, ptr|
      sender.check = (@tabbook.tabStyle == TABBOOK_BOTTOMTABS)
    end
    
    leftTabsCmd = FXMenuRadio.new(tabmenu, "&Left Tabs")
    leftTabsCmd.connect(SEL_COMMAND) do
      @tabbook.tabStyle = TABBOOK_LEFTTABS
      @tab1.tabOrientation = TAB_LEFT
      @tab2.tabOrientation = TAB_LEFT
      @tab3.tabOrientation = TAB_LEFT
    end
    leftTabsCmd.connect(SEL_UPDATE) do |sender, sel, ptr|
      sender.check = (@tabbook.tabStyle == TABBOOK_LEFTTABS)
    end
    
    rightTabsCmd = FXMenuRadio.new(tabmenu, "&Right Tabs")
    rightTabsCmd.connect(SEL_COMMAND) do
      @tabbook.tabStyle = TABBOOK_RIGHTTABS
      @tab1.tabOrientation = TAB_RIGHT
      @tab2.tabOrientation = TAB_RIGHT
      @tab3.tabOrientation = TAB_RIGHT
    end
    rightTabsCmd.connect(SEL_UPDATE) do |sender, sel, ptr|
      sender.check = (@tabbook.tabStyle == TABBOOK_RIGHTTABS)
    end
    
    FXMenuSeparator.new(tabmenu)
    
    addTabCmd = FXMenuCommand.new(tabmenu, "Add Tab")
    addTabCmd.connect(SEL_COMMAND) do
      FXTabItem.new(@tabbook, "New Tab")
      FXHorizontalFrame.new(@tabbook, FRAME_THICK|FRAME_RAISED) do |hf|
        FXLabel.new(hf, "Always add tab item and contents together.", :opts => LAYOUT_FILL)
      end
      @tabbook.create # realize widgets
      @tabbook.recalc # mark parent layout dirty
    end
    
    removeTabCmd = FXMenuCommand.new(tabmenu, "Remove Last Tab")
    removeTabCmd.connect(SEL_COMMAND) do
      numTabs = @tabbook.numChildren/2
      doomedTab = numTabs - 1
      @tabbook.removeChild(@tabbook.childAtIndex(2*doomedTab+1))
      @tabbook.removeChild(@tabbook.childAtIndex(2*doomedTab))
    end

    FXMenuTitle.new(menubar, "&Tab Placement", nil, tabmenu)
    
    main_listen
  end

  def create
    super
    show(PLACEMENT_SCREEN)
  end

end

if __FILE__ == $0
  # Make an application
  application = FXApp.new("TabBook", "FoxTest")

  # Build the main window
  TabBookWindow.new(application)

  # Create the application and its windows
  application.create

  # Run
  application.run
end
