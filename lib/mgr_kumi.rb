class MgrKumi
  attr_reader :id, :kumis, :fcodes, :kanchis, :memo, :rows_max

  def initialize db
    @db = db
    @arr_select = nil
  end
  
  def set_id sho_code
    @arr_select = @db.get_kumis(sho_code)
    @id = sho_code
    @kumis = @arr_select.size
    true
  end
  
  def nth i
    @kumi_sel = i
    data = @arr_select[i - 1]
    @fcodes = data[pos(:juzen)].split(/\|/)
    @kanchis = data[pos(:kanchi)].split(/\|/)
    @memo = data[pos(:memo)].split(/\|/)
    @rows_max = [@fcodes.size, @kanchis.size].max
    self
  end
  
  def pos col_sym
    [[:id, 0],
     [:juzen, 1],
     [:kanchi, 2],
     [:memo, 3]].assoc(col_sym)[1]
  end
  
end