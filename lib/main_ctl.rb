# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.

module MainCtl
  require './kanri_mgr'
  def main_listen
    kmgr = KanriMgr.new
    @tab11_simplelist.connect(SEL_CHANGED) do |obj, n|
      kana = @tab11_simplelist.getItem(obj.currentItem)
      arr = kmgr.obj_db.sho_get_by_kana(kana)
      #@tab11_simplelist.setItem(obj.currentItem,FXListItem.new("***"))
      @tab12_list.clearItems
      arr.each do |a|
        id = a[0]
        kmgr.prepare(id)
        kumis = kmgr.kumi.kumis
        str =  "#{id} : #{a[2]} (#{kumis})"
        @tab12_list.appendItem(str)
      end
    end
    
    #所有者リスト ################################################
    @tab12_list.connect(SEL_DOUBLECLICKED) do |obj|
      str = @tab12_list.getItem(obj.currentItem).text
      id, name = str.split(/ : /)
      @tab2_label.text = str
      @tabbook.setCurrent(1)
      kmgr.prepare(id)
      kumis = kmgr.kumi.kumis  #refers to size
      arr_row_ctl = []
      ctr = 0
      1.upto(kumis) do |n|
        kumi_sel = kmgr.kumi.nth n
        rows =  kumi_sel.rows_max
        arr_row_ctl << [ctr,rows]
        ctr += rows + 1  #1行空ける
      end
      arr_row_ctl.each.with_index(1) do |arr, i|
        ctr, rows_max = arr
#        @table_kan.clearItems()
        @table_kan.setRowText(ctr, i.to_s)
        kmgr.kumi.nth(i).fcodes.each_with_index do |fcode, j|
          cho, chiban = kmgr.juzen.get_cho_chiban(fcode)
          row = ctr + j
          @table_kan.setItemText(row,0, cho)
          @table_kan.setItemText(row,1, chiban)
        end
        kmgr.kumi.nth(i).kanchis.each_with_index do |kan, j|
          blk, lot = kmgr.kanchi.get_blk_lot(kan)
          row = ctr + j
          @table_kan.setItemText(row,3, blk)
          @table_kan.setItemText(row,4, lot)
        end
      end
    end
  end
end